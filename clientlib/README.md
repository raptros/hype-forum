clientlib
=========

the client library provides support for client frontend apps;
apps should be able to do all their interactions with servers entirely through this library.

goals
-----
* standard operations
  * login
  * fetch and post
  * disconnects
  
* local sqlite storage of data
  * so that eg files, images, threads, etc only have to be retrieved once
  * means protocol is needed for discovering post edits and user updates ... etc

* sensible interface that UIs can work with

* handles multiple servers
  * accounts for each

* mobile support
  * ie can handle constant disconnects elegantly
  * power saving features
  * push notifications
  * support for interacting with platform provided DBs on android and sqlite
    
* etc
  * admin tools?
  * plugin support?