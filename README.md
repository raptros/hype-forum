hype-forum
==========

rpc/native forum system

organization
------------

* `proto/` is the crate that contains the shared protocol spec
* `clientlib/` will contain a libary for creating client apps with
* `server/` implements a pg server
* `cli/` implements the end-user terminal client


justifying signed int64s as ids
-------------------------------
2^63 = 9,223,372,036,854,775,808.

to put this into perspective:
* the post id hsa that many possible values, basically. 
* let's say your forum was so popular that it got a billion new posts every day - and posts include
  replies, votes, reacts, etc.
* that's an enormous amount of daily traffic, right?
* it would take about 9 billion days of that much traffic to run out of post ids.
* note that a billion days is more than 2.7 million years. 
* at which point you can probably expect that not only will the schema have undergone revision,
  but also that your forum will probably be someone else's problem.