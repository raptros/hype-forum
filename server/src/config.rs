// config module

use crate::message_crypt::{KeySource, MessageCrypt};
use std::net::SocketAddr;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "hype-forum-server", about = "the hype-forum server")]
/// configure your hype-forum gRPC server.
pub struct ServerConfig {
    #[structopt(long, env = "HYPE_FORUM_ADDRESS", default_value = "127.0.0.1:3050")]
    /// GRPC server will be started listening on this address. note that it must be an IP address.
    pub address: SocketAddr,

    // note that the env var DATABASE_URL is the same one diesel_cli uses.
    #[structopt(long, env)]
    /// pass a database URL. must be a postgres database. make sure the database is fully migrated for the current version.
    pub database_url: String,

    /// pass in an AES session key directly.
    /// please don't use this on an actual deployment unless you're very sure that doing so will not leak the key data.
    #[structopt(long, env, hide_env_values = true)]
    pub session_aes_key: Option<String>,

    /// the better way to pass an AES key for session token encryption - a file.
    /// you should use this
    /// todo: figure out how to encrypt the file
    #[structopt(
        long,
        env,
        required_unless = "session-aes-key",
        conflicts_with = "session-aes-key",
        parse(from_os_str)
    )]
    pub session_aes_key_file: Option<PathBuf>,

    /// server epoch. any time you change the session key, you can update this.
    /// the idea is that we only want to use 32 bits for the date-time part of the session token nonce
    #[structopt(long, env, default_value = "2021-05-01T00:00:00Z")]
    pub server_epoch: chrono::DateTime<chrono::Utc>,

    /// you can optionally set a server id. it's not necessary; the primary use is for providing
    /// sufficient uniqueness for the init vector for session tokens. if you are going to have a lot
    /// of machines and you're worried about RNG collision, feel free to set this.
    #[structopt(long, env)]
    pub server_id: Option<u32>,

    /// this is a nonce used for hashing email addresses.
    /// if you change it, every email hash stored in your database will be invalid.
    /// be careful!
    /// the purpose of using a nonce for email hashes is that if anyone does manage to steal your database,
    /// you want to prevent people from being able to check if any particular address is stored in your database.
    /// this means you should keep the nonce secure!
    #[structopt(long, env)]
    pub email_hash_nonce: String,

    /// pass in an AES session key directly.
    /// please don't use this on an actual deployment unless you're very sure that doing so will not leak the key data.
    #[structopt(long, env, hide_env_values = true)]
    pub email_aes_key: Option<String>,

    /// the better way to pass an AES key for email address encryption - a file.
    /// you should use this.
    /// this is used to store email addresses encrypted in the database.
    /// we are not giving you a choice on this one - you will store your user email addresses securely.
    /// in fact this key will be used for any record that's
    /// todo: figure out how to encrypt the file
    #[structopt(
        long,
        env,
        required_unless = "email-aes-key",
        conflicts_with = "email-aes-key",
        parse(from_os_str)
    )]
    pub email_aes_key_file: Option<PathBuf>,
}

impl ServerConfig {
    fn get_message_crypt(
        &self,
        key_param: Option<&String>,
        file_param: Option<&PathBuf>,
        key_name: &str,
    ) -> anyhow::Result<MessageCrypt> {
        let epoch = self.server_epoch.clone();
        let server_id = self.server_id.clone();
        let key_source = if let Some(key_string) = key_param {
            KeySource::Raw(key_string)
        } else if let Some(key_file) = file_param {
            KeySource::File(key_file.as_path())
        } else {
            anyhow::bail!("config did not prove key for {}", key_name)
        };
        MessageCrypt::try_new(key_source, epoch, server_id)
    }

    pub fn get_session_crypt(&self) -> anyhow::Result<MessageCrypt> {
        self.get_message_crypt(
            self.session_aes_key.as_ref(),
            self.session_aes_key_file.as_ref(),
            "sessions",
        )
    }

    pub fn get_email_crypt(&self) -> anyhow::Result<MessageCrypt> {
        self.get_message_crypt(
            self.email_aes_key.as_ref(),
            self.email_aes_key_file.as_ref(),
            "emails",
        )
    }
}
