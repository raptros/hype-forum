use hype_forum_server::config::ServerConfig;
use std::convert::TryInto;
use structopt::StructOpt;
use tracing_subscriber::fmt::format::FmtSpan;

// todo: executor config
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // load .env
    let env_result = dotenv::dotenv();

    // this uses RUST_LOG env var
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        // todo: this logs at info, which isn't great really.
        .with_span_events(FmtSpan::CLOSE)
        .init();

    // report on dotenv load without killing the app.
    match env_result {
        Ok(p) => tracing::info!(path = ?p, "using env file"),
        Err(e) => tracing::info!(error = ?e, "not using env file"),
    };

    let config = ServerConfig::from_args();
    let context = config.try_into()?;
    hype_forum_server::run(context).await
}
