use std::marker::Copy;

use aes_gcm::{AeadCore, AeadInPlace, Aes256Gcm, Key, NewAead, Nonce};
use byteorder::ByteOrder;
use generic_array::GenericArray;
use prost::Message;
use rand::RngCore;
use typenum::Unsigned;

use anyhow::Context;
use chrono::{DateTime, Utc};
use lazy_static::lazy_static;
use sha3::{Digest, Sha3_256};
use std::path::Path;
use std::sync::Arc;

#[allow(dead_code)]
#[derive(Debug)]
pub struct ByteCounts {
    iv: usize,
    tag: usize,
    overhead: usize,
    total: usize,
}

impl ByteCounts {
    fn get_aes_256_gcm() -> Self {
        // if to_usize() was a const method, I could use a const instead of lazy_static, but no
        let iv = <<Aes256Gcm as AeadCore>::NonceSize as Unsigned>::to_usize();
        let tag = <<Aes256Gcm as AeadCore>::TagSize as Unsigned>::to_usize();
        let overhead = <<Aes256Gcm as AeadCore>::CiphertextOverhead as Unsigned>::to_usize();

        let total = iv + tag + overhead;

        ByteCounts {
            iv,
            tag,
            overhead,
            total,
        }
    }
}

lazy_static! {
    static ref AES_256_GCM_BYTE_COUNTS: ByteCounts = ByteCounts::get_aes_256_gcm();
}

#[derive(Debug, Copy, Clone)]
pub struct CryptError;

pub enum KeySource<'a> {
    Raw(&'a str),
    File(&'a Path),
}

impl<'a> KeySource<'a> {
    pub fn load(self) -> Result<Aes256Gcm, anyhow::Error> {
        let key_size = <<Aes256Gcm as NewAead>::KeySize as Unsigned>::to_usize();
        let key = match self {
            KeySource::Raw(key_string) => key_string.as_bytes().to_vec(),
            KeySource::File(key_file) => {
                use std::fs::File;
                use std::io::prelude::*;
                let mut file = File::open(key_file)
                    .with_context(|| format!("trying to read key bytes from {:?}", key_file))?;
                let mut buffer = Vec::with_capacity(key_size);
                let n = file.read(&mut buffer).context("reading bytes from file")?;
                if n < key_size {
                    anyhow::bail!(
                        "key file {:?} only contained {} bytes, needed {}",
                        key_file,
                        n,
                        key_size
                    );
                }
                buffer
            }
        };
        if key.len() != key_size {
            anyhow::bail!("key is {} bytes, needs to be {}", key.len(), key_size);
        }
        let key = Key::from_slice(key.as_slice());
        Ok(aes_gcm::Aes256Gcm::new(key))
    }
}

#[derive(Clone)]
pub struct MessageCrypt {
    cipher: Arc<Aes256Gcm>,
    epoch: DateTime<Utc>,
    id: Option<u32>,
}

impl MessageCrypt {
    pub fn try_new(
        source: KeySource,
        epoch: DateTime<Utc>,
        id: Option<u32>,
    ) -> Result<Self, anyhow::Error> {
        let cipher = Arc::new(source.load()?);
        Ok(Self { cipher, epoch, id })
    }

    pub fn encrypt_message<M: Message>(&self, message: &M) -> Result<Vec<u8>, CryptError> {
        let message_size = message.encoded_len();

        // the layout of the token: iv, tag, message, overhead
        let mut result_vec = vec![0_u8; AES_256_GCM_BYTE_COUNTS.total + message_size];

        // write in first part of nonce - timestamp
        let mut iv_bytes_needed = AES_256_GCM_BYTE_COUNTS.iv;
        {
            let now = Utc::now();
            let offset = now - self.epoch;
            // write the 4 least significant bytes into the beginning of result_vec,
            // in little endian order. the point is really to get sufficient uniqueness
            let source = &offset.num_milliseconds().to_le_bytes()[0..4];
            result_vec[0..4].copy_from_slice(source);
            iv_bytes_needed -= source.len();
        }
        // write the rest of the nonce
        if let Some(server_id) = self.id {
            // write server id
            byteorder::BigEndian::write_u32(&mut result_vec[4..8], server_id);
            iv_bytes_needed -= 4;
        }
        {
            let start = AES_256_GCM_BYTE_COUNTS.iv - iv_bytes_needed;
            // and write however many bytes are needed
            rand::thread_rng()
                .try_fill_bytes(&mut result_vec[start..AES_256_GCM_BYTE_COUNTS.iv])
                .map_err(|error| {
                    tracing::error!(?error, "rng failure");
                    CryptError
                })?;
        }
        // now write the message into the vec
        {
            // the message lives in the space after the iv and the tag
            let mut buf =
                &mut result_vec[AES_256_GCM_BYTE_COUNTS.iv + AES_256_GCM_BYTE_COUNTS.tag..];
            message.encode(&mut buf).map_err(|error| {
                tracing::error!(?error, "proto encoding error");
                CryptError
            })?;
        }
        // and encrypt
        {
            // split off the nonce
            let (nonce_part, rest_part) = result_vec
                .as_mut_slice()
                .split_at_mut(AES_256_GCM_BYTE_COUNTS.iv);
            // then split space for the tag before the message
            let (tag_part, message_part) = rest_part.split_at_mut(AES_256_GCM_BYTE_COUNTS.tag);

            // it's very important that this is the right size
            let nonce = Nonce::from_slice(nonce_part);
            let tag = self
                .cipher
                .encrypt_in_place_detached(nonce, b"", message_part)
                .map_err(|error| {
                    tracing::error!(?error, "encryption error");
                    CryptError
                })?;
            tag_part.copy_from_slice(tag.as_slice());
        };
        Ok(result_vec)
    }

    pub fn decrypt_message<M: Message + Default>(
        &self,
        mut ciphertext: Vec<u8>,
    ) -> Result<M, CryptError> {
        // the reason we do not also include the size of M here is that protobuf-encoding is different from rust memory layout,
        // which means that mem::sizeof::<M>() is not going to be the same as the protobuf size (note that proto can for instance trim numbers).
        // in any case, if the ciphertext is too small for just the cipher itself, bail.
        if ciphertext.len() < AES_256_GCM_BYTE_COUNTS.total {
            tracing::debug!("received ciphertext below minimum size for decoding");
            return Err(CryptError);
        }
        // separate references to the nonce and the message
        let (nonce_part, tag_and_message_part) = ciphertext
            .as_mut_slice()
            .split_at_mut(AES_256_GCM_BYTE_COUNTS.iv);

        // also separate out the tag from the message.
        let (tag_part, mut message_part) =
            tag_and_message_part.split_at_mut(AES_256_GCM_BYTE_COUNTS.tag);

        let nonce = Nonce::from_slice(nonce_part);
        let tag = GenericArray::from_slice(tag_part);

        self.cipher
            .decrypt_in_place_detached(nonce, b"", &mut message_part, tag)
            .map_err(|_| {
                // decryption could fail for all sorts of reason. a big one: someone is sending a bogus token!
                // the Error type from the aes crypt actually does not contain any information.
                tracing::debug!("decryption failed");
                CryptError
            })?;

        M::decode(&*message_part).map_err(|e| {
            tracing::error!(error = ?e, "decoding failed!");
            CryptError
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct HashError;

pub type HashResult = Result<Vec<u8>, HashError>;

pub trait SecureHashExt {
    fn secure_hash_reset(&self, hasher: &mut Sha3_256) -> HashResult {
        let data = self.encode_to_buf()?;
        hasher.update(data);
        Ok(hasher.finalize_reset().to_vec())
    }

    fn secure_hash(&self) -> HashResult {
        let data = self.encode_to_buf()?;
        Ok(Sha3_256::digest(data.as_slice()).to_vec())
    }

    fn encode_to_buf(&self) -> HashResult;
}

impl<M: Message + Sized> SecureHashExt for M {
    fn encode_to_buf(&self) -> HashResult {
        let mut buf = Vec::with_capacity(self.encoded_len());
        self.encode(&mut buf).map_err(|error| {
            tracing::error!(?error, "failed to encode to allocated buffer");
            HashError
        })?;
        Ok(buf)
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use chrono::{DateTime, Utc};
    use test_env_log::test;

    use hype_forum_protos::hype_forum::account::SessionIndexData;
    use hype_forum_protos::hype_forum::SessionTokenInner;

    use crate::message_crypt::{KeySource, MessageCrypt, SecureHashExt};
    use sha3::{Digest, Sha3_256};

    fn test_crypt() -> MessageCrypt {
        let key_string = "a".repeat(32);
        let key_source = KeySource::Raw(key_string.as_str());
        let epoch: DateTime<Utc> =
            DateTime::from_str("2021-05-01T00:00:00Z").expect("this should parse lol");
        MessageCrypt::try_new(key_source, epoch, None).expect("should have loaded")
    }

    #[test]
    fn round_trip_message() {
        let crypt = test_crypt();
        let expires_at = Some(Utc::now().into());
        let message = SessionTokenInner {
            session_id: 123,
            user_id: 456,
            expires_at,
            login_type: 1,
        };

        let crypto_text = crypt
            .encrypt_message(&message)
            .expect("failed to encrypt message!");

        let plaintext = crypt
            .decrypt_message::<SessionTokenInner>(crypto_text)
            .expect("failed to decrypt message!");

        assert_eq!(
            message, plaintext,
            "original was {:?}, result was {:?}",
            message, plaintext
        );
    }

    #[test]
    fn hash_repeatedly() {
        let created_at = Some(Utc::now().into());
        let message = SessionIndexData {
            session_id: 1,
            user_id: 2,
            created_at,
        };

        let hash1 = message.clone().secure_hash().expect("no hashing issues");

        let mut hasher = Sha3_256::new();
        let hash2 = message
            .clone()
            .secure_hash_reset(&mut hasher)
            .expect("no hashing issues");

        assert_eq!(hash1, hash2);

        let message2 = SessionIndexData {
            session_id: 222,
            user_id: 3252,
            created_at: None,
        };

        let hash3 = message2.secure_hash().expect("no hashing issues");

        assert_ne!(hash1, hash3);

        let hash4 = message2
            .secure_hash_reset(&mut hasher)
            .expect("no hashing issues");

        assert_eq!(hash3, hash4);
    }

    #[test]
    fn round_trip_crypt_hash_match() {
        let crypt = test_crypt();
        let created_at = Some(Utc::now().into());
        let message = SessionIndexData {
            session_id: 1,
            user_id: 2,
            created_at,
        };

        let hash1 = message.clone().secure_hash().expect("no hashing issues");

        let crypto_text = crypt
            .encrypt_message(&message)
            .expect("failed to encrypt message!");

        let plaintext = crypt
            .decrypt_message::<SessionIndexData>(crypto_text)
            .expect("failed to decrypt message!");

        let hash2 = plaintext.secure_hash().expect("no hashing issues");

        assert_eq!(hash1, hash2)
    }
}
