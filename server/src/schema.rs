table! {
    account_emails (email_hash) {
        email_hash -> Bytea,
        user_id -> Int8,
        encrypted_email -> Bytea,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    account_passwords (user_id) {
        user_id -> Int8,
        hash -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    boards (id) {
        id -> Int8,
        parent_id -> Nullable<Int8>,
        display_name -> Nullable<Varchar>,
        banner -> Nullable<Varchar>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    posts (id) {
        id -> Int8,
        thread_id -> Int8,
        user_id -> Int8,
        post_text -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    session_log (id) {
        id -> Int8,
        session_id -> Int8,
        ip -> Nullable<Varchar>,
        user_agent -> Nullable<Varchar>,
        service -> Varchar,
        method -> Varchar,
        created_at -> Timestamptz,
    }
}

table! {
    sessions (id) {
        id -> Int8,
        user_id -> Int8,
        is_valid -> Bool,
        login_type -> Int4,
        ip -> Nullable<Varchar>,
        user_agent -> Nullable<Varchar>,
        expires_at -> Nullable<Timestamptz>,
        created_at -> Timestamptz,
    }
}

table! {
    threads (id) {
        id -> Int8,
        board_id -> Int8,
        poster_id -> Int8,
        display_title -> Varchar,
        about -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    user_accounts (user_id) {
        user_id -> Int8,
        user_slug -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    users (id) {
        id -> Int8,
        display_name -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(account_emails -> users (user_id));
joinable!(account_passwords -> users (user_id));
joinable!(posts -> threads (thread_id));
joinable!(posts -> users (user_id));
joinable!(session_log -> sessions (session_id));
joinable!(sessions -> users (user_id));
joinable!(threads -> boards (board_id));
joinable!(threads -> users (poster_id));
joinable!(user_accounts -> users (user_id));

allow_tables_to_appear_in_same_query!(
    account_emails,
    account_passwords,
    boards,
    posts,
    session_log,
    sessions,
    threads,
    user_accounts,
    users,
);
