#[inline]
pub fn require<T, E, P: FnOnce(&T) -> bool, D: FnOnce() -> E>(
    t: T,
    default: D,
    predicate: P,
) -> Result<T, E> {
    if predicate(&t) {
        Ok(t)
    } else {
        Err(default())
    }
}

pub trait ResultFilterExt<T, E> {
    /// the signature of this puts default before predicate to match the signature of Result::map_or_else
    fn require_or_else<P, D>(self, default: D, predicate: P) -> Result<T, E>
    where
        P: FnOnce(&T) -> bool,
        D: FnOnce() -> E;
}

impl<T, E> ResultFilterExt<T, E> for Result<T, E> {
    #[inline]
    fn require_or_else<P, D>(self, default: D, predicate: P) -> Result<T, E>
    where
        P: FnOnce(&T) -> bool,
        D: FnOnce() -> E,
    {
        self.and_then(|v| require(v, default, predicate))
    }
}
