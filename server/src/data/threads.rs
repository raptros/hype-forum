use chrono::DateTime;
use derive_more::Display;
use diesel::result::DatabaseErrorKind;
use diesel::result::Error as DieselError;
use diesel::OptionalExtension;
use diesel::{BoxableExpression, ExpressionMethods, QueryDsl, RunQueryDsl};
use diesel_derive_newtype::DieselNewType;
use tracing::instrument;

use hype_forum_protos::hype_forum::threads::get_posts_request::Cursor;
use hype_forum_protos::hype_forum::threads::{ThreadInfo, ThreadPost, ThreadPostBody};
use hype_forum_protos::hype_forum::CursorDirection;

use crate::data::boards::BoardId;
use crate::data::users::UserId;
use crate::data::{DataError, HasPool, Limit, LimitDslExt};
use crate::schema::posts;
use crate::schema::threads;
use crate::ServerContext;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType, Display)]
#[display(fmt = "t:{}", _0)]
pub struct ThreadId(pub i64);

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType, Display)]
#[display(fmt = "p:{}", _0)]
pub struct PostId(pub(in crate::data) i64);

#[derive(Queryable, Identifiable, Debug)]
pub struct Thread {
    pub id: ThreadId,

    pub board_id: BoardId,

    pub poster_id: UserId,

    pub display_title: String,

    pub about: String,

    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "threads"]
pub struct NewThread {
    pub board_id: BoardId,

    pub poster_id: UserId,

    pub display_title: String,

    pub about: String,
}

#[derive(Queryable, Identifiable, Debug)]
pub struct Post {
    pub id: PostId,

    pub thread_id: ThreadId,

    pub user_id: UserId,

    pub post_text: String,

    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "posts"]
pub struct NewPost {
    pub thread_id: ThreadId,

    pub user_id: UserId,

    pub post_text: String,
}

#[tonic::async_trait]
pub trait ThreadsPosts: HasPool {
    #[instrument(skip(self))]
    async fn get_thread(&self, target_thread_id: ThreadId) -> Result<Option<Thread>, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            threads::table
                .find(target_thread_id)
                .first(conn)
                .optional()
                .map_err(|error| {
                    tracing::error!(?error, thread_id = %target_thread_id, "thread query failed");
                    DataError
                })
        })
        .await
    }

    #[instrument(skip(self))]
    async fn post_thread(
        &self,
        thread_req: NewThread,
        first_post_text: String,
    ) -> Result<(Thread, Post), DataError> {
        self.spawn_transaction(move |conn| {
            let thread: Thread = diesel::insert_into(threads::table)
                .values(thread_req)
                .get_result(conn)
                .map_err(|error| {
                    tracing::error!(?error, "thread insert failed");
                    DataError
                })?;
            let new_post = NewPost {
                thread_id: thread.id,
                user_id: thread.poster_id,
                post_text: first_post_text,
            };
            let post = diesel::insert_into(posts::table)
                .values(new_post)
                .get_result(conn)
                .map_err(|error| {
                    tracing::error!(?error, "post insert failed");
                    DataError
                })?;
            Ok((thread, post))
        })
        .await
    }

    #[instrument(skip(self))]
    async fn list_posts(
        &self,
        target_thread_id: ThreadId,
        mut cursor: ThreadPostCursor,
    ) -> Result<Vec<Post>, DataError> {
        use crate::schema::posts::dsl::*;
        self.spawn_blocking_with_conn(move |conn| {
            let mut query = posts.filter(thread_id.eq(target_thread_id)).into_boxed();
            // the default is to get from the start of the thread.
            let sort: Box<dyn BoxableExpression<posts, _, SqlType = ()>> =
                if cursor.direction == CursorDirection::EndingBefore {
                    Box::new(id.asc())
                } else {
                    Box::new(id.desc())
                };
            if let Some(target_post) = cursor.post_id.take() {
                match cursor.direction {
                    CursorDirection::None => (),
                    CursorDirection::StartingAfter => {
                        query = query.filter(id.gt(target_post));
                    }
                    CursorDirection::EndingBefore => {
                        query = query.filter(id.lt(target_post));
                    }
                }
            };
            query
                .limit_n(cursor.limit)
                .order_by(sort)
                .load(conn)
                .map_err(|error| {
                    tracing::error!(?error, "failed to load posts");
                    DataError
                })
        })
        .await
    }

    #[instrument(skip(self))]
    async fn insert_post(&self, new_post: NewPost) -> Result<Option<Post>, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            let post_result = diesel::insert_into(posts::table)
                .values(new_post)
                .get_result(conn);

            match post_result {
                Ok(r) => Ok(Some(r)),
                // if there's a foreign key constraint issue, then the target thread didn't exist
                Err(DieselError::DatabaseError(DatabaseErrorKind::ForeignKeyViolation, _)) => {
                    Ok(None)
                }
                Err(error) => {
                    tracing::error!(?error, "failed to post reply");
                    Err(DataError)
                }
            }
        })
        .await
    }
}

impl ThreadsPosts for ServerContext {}

impl From<Thread> for ThreadInfo {
    fn from(t: Thread) -> Self {
        Self {
            thread_id: t.id.0,
            poster_id: t.poster_id.0,
            board_id: t.board_id.0,
            title: t.display_title,
            about: t.about,
        }
    }
}

impl From<Post> for ThreadPost {
    fn from(p: Post) -> Self {
        Self {
            id: p.id.0,
            thread_id: p.thread_id.0,
            user_id: p.user_id.0,
            // todo: metadata
            metadata: None,
            // this is going to require changes in the future lol.
            body: Some(ThreadPostBody {
                post_text: p.post_text,
            }),
            // todo: reactions. somehow.
            reactions: None,
        }
    }
}

#[derive(Debug)]
pub struct ThreadPostCursor {
    direction: CursorDirection,
    post_id: Option<PostId>,
    limit: Limit,
}

impl From<Cursor> for ThreadPostCursor {
    fn from(c: Cursor) -> Self {
        Self {
            direction: c.direction(),
            post_id: c.post_id.map(PostId),
            limit: c.limit.into(),
        }
    }
}

impl Default for ThreadPostCursor {
    fn default() -> Self {
        Self {
            direction: CursorDirection::None,
            post_id: None,
            limit: Limit::default(),
        }
    }
}
