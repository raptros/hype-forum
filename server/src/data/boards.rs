use derive_more::Display;
use diesel_derive_newtype::DieselNewType;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType, Display)]
#[display(fmt = "b:{}", _0)]
pub struct BoardId(pub(crate) i64);
