// this module is for user data!

use chrono::DateTime;
use derive_more::Display;
use diesel::OptionalExtension;
use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
use diesel_derive_newtype::DieselNewType;
use tracing::instrument;

use hype_forum_protos::hype_forum::UserInfo;

use crate::data::{DataError, HasPool};
use crate::schema::users;
use crate::ServerContext;

pub mod accounts;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType, Display)]
#[display(fmt = "u:{}", _0)]
pub struct UserId(pub(crate) i64);

#[derive(Queryable, Identifiable, Debug)]
pub struct User {
    pub id: UserId,
    pub display_name: String,
    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser {
    pub display_name: String,
}

#[tonic::async_trait]
pub trait Users: HasPool {
    #[instrument(skip(self))]
    async fn get_user(&self, user_id: UserId) -> Result<Option<User>, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            users::table
                .find(user_id)
                .first(conn)
                .optional()
                .map_err(|error| {
                    tracing::error!(?error, "user query failed");
                    DataError
                })
        })
        .await
    }

    #[instrument(skip(self))]
    async fn lookup_user_login(&self, user_string: String) -> Result<Option<User>, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            users::table
                .filter(users::display_name.eq(user_string))
                .first::<User>(conn)
                .optional()
                .map_err(|error| {
                    tracing::error!(?error, "user lookup failed");
                    DataError
                })
        })
        .await
    }

    #[instrument(skip(self))]
    async fn insert_user(&self, new_user: NewUser) -> Result<User, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            diesel::insert_into(users::table)
                .values(new_user)
                .get_result(conn)
                .map_err(|error| {
                    tracing::error!(?error, "user insert failed");
                    DataError
                })
        })
        .await
    }
}

#[tonic::async_trait]
impl Users for ServerContext {}

impl From<User> for UserInfo {
    fn from(u: User) -> Self {
        UserInfo {
            display_name: u.display_name,
        }
    }
}
