// this module is for account data!

use chrono::DateTime;
use diesel::OptionalExtension;
use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
use tracing::instrument;

use crate::data::users::{NewUser, User, UserId};
use crate::data::wrappers::{EmailHash, ParsedEmailAddress, UserSlug};
use crate::data::{Conn, DataError, DataResult, HasPool};
use crate::schema::account_emails;
use crate::schema::account_passwords;
use crate::schema::user_accounts;
use crate::schema::users;
use crate::ServerContext;

#[derive(Queryable, Identifiable, Debug)]
#[primary_key(user_id)]
pub struct UserAccount {
    pub user_id: UserId,
    pub user_slug: UserSlug,
    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "user_accounts"]
pub struct NewUserAccount {
    pub user_id: UserId,
    pub user_slug: UserSlug,
}

#[derive(Queryable, Identifiable, Debug)]
#[primary_key(email_hash)]
pub struct AccountEmail {
    pub email_hash: EmailHash,
    pub user_id: UserId,
    // todo
    pub encrypted_email_bytes: Vec<u8>,
    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "account_emails"]
pub struct NewAccountEmail {
    pub email_hash: EmailHash,
    pub user_id: UserId,
    // todo
    pub encrypted_email: Vec<u8>,
}

#[derive(Queryable, Identifiable)]
#[primary_key(user_id)]
pub struct AccountPassword {
    pub user_id: UserId,
    pub hash: String,
    pub created_at: DateTime<chrono::Utc>,
    pub updated_at: DateTime<chrono::Utc>,
}

#[derive(Queryable, Identifiable, Insertable)]
#[table_name = "account_passwords"]
#[primary_key(user_id)]
pub struct NewAccountPassword {
    pub user_id: UserId,
    pub hash: String,
}

pub struct LoadedUserAccount {
    pub user: User,
    pub account: UserAccount,
    pub password: AccountPassword,
    pub emails: Vec<AccountEmail>,
}

#[tonic::async_trait]
pub trait Accounts: HasPool {
    #[instrument(skip(self))]
    async fn load_account(&self, user_string: String) -> DataResult<Option<LoadedUserAccount>> {
        self.spawn_blocking_with_conn(move |conn| {
            let user_slug = UserSlug::new(&user_string);
            match user_string.parse::<ParsedEmailAddress>() {
                Err(_) => get_user_account_by_slug(conn, &user_slug),

                // todo: actually get the nonce here!
                Ok(address) => {
                    lookup_user_by_email(conn, &EmailHash::new((&address).into(), "yeet"))?
                        .map_or_else(
                            // try looking up the account directly
                            || get_user_account_by_slug(conn, &user_slug),
                            // if an account id was found via email, then get the account
                            |id| get_user_account(conn, &id),
                        )
                }
            }
        })
        .await
    }

    #[instrument(skip(self, password_hash, encrypted_email))]
    async fn register(
        &self,
        display_name: String,
        password_hash: String,
        email_hash: EmailHash,
        encrypted_email: Vec<u8>,
    ) -> DataResult<User> {
        self.spawn_transaction(move |conn| {
            // todo: figure out how to validate that the user name will be valid and report it for the caller!

            let new_user = NewUser { display_name };
            let created_user = diesel::insert_into(users::table)
                .values(new_user)
                .get_result::<User>(conn)
                .map_err(|error| {
                    tracing::error!(?error, "user insert failed");
                    DataError
                })?;

            let user_id = created_user.id;

            let new_account = NewUserAccount {
                user_id,
                user_slug: UserSlug::new(&created_user.display_name),
            };

            diesel::insert_into(user_accounts::table)
                .values(new_account)
                .execute(conn)
                .map_err(|error| {
                    tracing::error!(?error, "account insert failed");
                    DataError
                })?;

            let new_password = NewAccountPassword {
                user_id,
                hash: password_hash,
            };

            diesel::insert_into(account_passwords::table)
                .values(new_password)
                .execute(conn)
                .map_err(|error| {
                    tracing::error!(?error, "password insert failed");
                    DataError
                })?;

            let new_email = NewAccountEmail {
                email_hash,
                user_id,
                encrypted_email,
            };

            diesel::insert_into(account_emails::table)
                .values(new_email)
                .execute(conn)
                .map_err(|error| {
                    tracing::error!(?error, "email insert failed");
                    DataError
                })?;

            Ok(created_user)
        })
        .await
    }
}

fn lookup_user_by_email(conn: &Conn, hash: &EmailHash) -> DataResult<Option<UserId>> {
    account_emails::table
        .select(account_emails::user_id)
        .find(hash)
        .first::<UserId>(conn)
        .optional()
        .map_err(|error| {
            tracing::error!(?error, "account email lookup failed");
            DataError
        })
}

fn get_user_account(conn: &Conn, user_id: &UserId) -> DataResult<Option<LoadedUserAccount>> {
    let result = users::table
        .inner_join(user_accounts::table)
        .inner_join(account_passwords::table)
        .filter(users::id.eq(user_id))
        .first::<(User, UserAccount, AccountPassword)>(conn)
        .optional()
        .map_err(|error| {
            // todo: this error mechanism is getting really annoying
            tracing::error!(?error, "user account lookup failed");
            DataError
        })?;

    let (user, account, password) = match result {
        None => return Ok(None),
        Some(r) => r,
    };

    let emails = account_emails::table
        .filter(account_emails::user_id.eq(user_id))
        .load::<AccountEmail>(conn)
        .map_err(|error| {
            // todo: this error mechanism is getting really annoying
            tracing::error!(?error, "user account email lookup failed");
            DataError
        })?;

    Ok(Some(LoadedUserAccount {
        user,
        account,
        password,
        emails,
    }))
}

fn get_user_account_by_slug(
    conn: &Conn,
    user_slug: &UserSlug,
) -> DataResult<Option<LoadedUserAccount>> {
    let result = user_accounts::table
        .inner_join(users::table.inner_join(account_passwords::table))
        .filter(user_accounts::user_slug.eq(user_slug))
        .first::<(UserAccount, (User, AccountPassword))>(conn)
        .optional()
        .map_err(|error| {
            // todo: this error mechanism is getting really annoying
            tracing::error!(?error, "user account lookup failed");
            DataError
        })?;

    let (account, (user, password)) = match result {
        None => return Ok(None),
        Some(r) => r,
    };

    let emails = account_emails::table
        .filter(account_emails::user_id.eq(user.id))
        .load::<AccountEmail>(conn)
        .map_err(|error| {
            // todo: this error mechanism is getting really annoying
            tracing::error!(?error, "user account email lookup failed");
            DataError
        })?;

    Ok(Some(LoadedUserAccount {
        user,
        account,
        password,
        emails,
    }))
}

#[tonic::async_trait]
impl Accounts for ServerContext {}
