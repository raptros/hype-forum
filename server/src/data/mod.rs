use diesel::query_dsl::methods::LimitDsl;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::result::Error;
use diesel::{Connection, PgConnection};
use r2d2::PooledConnection;
use tokio::task::JoinError;
use tonic::async_trait;
use tonic::Status;

use crate::ServerContext;

pub mod boards;
pub mod sessions;
pub mod threads;
pub mod users;
pub mod wrappers;

// empty struct for database errors
pub struct DataError;

impl From<JoinError> for DataError {
    fn from(error: JoinError) -> Self {
        tracing::error!(?error, "failed to join database task");
        DataError
    }
}

impl From<r2d2::Error> for DataError {
    fn from(error: r2d2::Error) -> Self {
        tracing::error!(?error, "failed to checkout database connection");
        DataError
    }
}

impl From<diesel::result::Error> for DataError {
    fn from(error: Error) -> Self {
        // todo: we need database error context info, actually
        //       maybe use anyhow?
        tracing::error!(?error, "database error");
        DataError
    }
}

impl From<DataError> for Status {
    fn from(_: DataError) -> Self {
        Status::internal("server error")
    }
}

pub type Conn = PooledConnection<ConnectionManager<PgConnection>>;

pub type DataResult<R> = Result<R, DataError>;

#[async_trait]
pub trait HasPool {
    fn get_pool(&self) -> &Pool<ConnectionManager<PgConnection>>;

    async fn spawn_transaction<R, F>(&self, f: F) -> Result<R, DataError>
    where
        F: FnOnce(&Conn) -> Result<R, DataError> + Send + 'static,
        R: Send + 'static,
    {
        self.spawn_blocking_with_conn(|conn| conn.transaction(|| f(conn)))
            .await
    }

    /// checks out a connection from the pool and provides it to a task running a blocking-suitable thread
    /// note that this function is only async so that it can handle checking a JoinHandle result (simplifying the return)
    async fn spawn_blocking_with_conn<R, F>(&self, f: F) -> Result<R, DataError>
    where
        F: FnOnce(&Conn) -> Result<R, DataError> + Send + 'static,
        R: Send + 'static,
    {
        let pool = self.get_pool().clone();
        tokio::task::spawn_blocking(move || {
            let conn = pool.get()?;
            f(&conn)
        })
        .await?
    }

    /// Non-async, non-backgrounding version of the previous. this uses tokio::task::block_in_place(),
    /// which is meant to move the current thread into the "blocking" section.
    fn block_in_place_with_conn<R, F>(&self, f: F) -> Result<R, DataError>
    where
        F: FnOnce(&Conn) -> Result<R, DataError>,
    {
        let pool = self.get_pool().clone();
        tokio::task::block_in_place(move || {
            let conn = pool.get()?;
            f(&conn)
        })
    }
}

#[async_trait]
impl HasPool for ServerContext {
    fn get_pool(&self) -> &Pool<ConnectionManager<PgConnection>> {
        &self.pool
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Limit(i64);

impl From<u32> for Limit {
    fn from(l: u32) -> Self {
        let v = if l == 0 || l > 100 { 100_i64 } else { l.into() };
        Self(v)
    }
}

impl Default for Limit {
    fn default() -> Self {
        Self(100)
    }
}

pub trait LimitDslExt: LimitDsl + Sized {
    fn limit_n(self, limit: Limit) -> <Self as LimitDsl>::Output {
        self.limit(limit.0)
    }
}

impl<T: LimitDsl + Sized> LimitDslExt for T {}
