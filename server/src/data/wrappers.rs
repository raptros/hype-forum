// module for wrapper typesp
use base64::display::Base64Display;
use derive_more::Display;
use diesel_derive_newtype::DieselNewType;
use email_address_parser::{EmailAddress, ParsingOptions};
use hype_forum_protos::hype_forum::StoredEmailAddress;
use sha3::{Digest, Sha3_256};
use std::fmt::{Display, Formatter};
use std::str::FromStr;
use tonic::Status;

#[derive(Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType, Display)]
#[display(fmt = "us:{}", _0)]
pub struct UserSlug(String);

impl UserSlug {
    pub fn new<S: AsRef<str>>(s: S) -> Self {
        // this removes whitespace. todo: remove punctuation as well
        let clean = s.as_ref().split_whitespace().map(|s| s.to_lowercase()).collect::<String>();
        UserSlug(clean)
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, AsExpression, DieselNewType)]
pub struct EmailHash(Vec<u8>);

// base 64 encoded writing of email hash.
impl Display for EmailHash {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let wrapper = Base64Display::with_config(self.0.as_slice(), base64::STANDARD);
        write!(f, "eh:{}", wrapper)
    }
}

impl EmailHash {
    pub fn new(address: NormalizedEmail, nonce: &str) -> Self {
        let mut hasher = Sha3_256::new();
        hasher.update(nonce.as_bytes());
        hasher.update(address.0.as_bytes());
        Self(hasher.finalize().to_vec())
    }

    pub fn parsed<S: AsRef<str>, S2: AsRef<str>>(
        address: S,
        nonce: S2,
    ) -> Result<Self, InvalidEmail> {
        let result = address.as_ref().parse::<ParsedEmailAddress>()?;
        Ok(Self::new((&result).into(), nonce.as_ref()))
    }
}

/// intermediate form for producing email hashes and for passing around
pub struct NormalizedEmail(String);

impl From<&ParsedEmailAddress> for NormalizedEmail {
    fn from(p: &ParsedEmailAddress) -> Self {
        Self::from(&p.0)
    }
}

impl From<&EmailAddress> for NormalizedEmail {
    fn from(a: &EmailAddress) -> Self {
        let domain_norm = a.get_domain().to_lowercase();
        let local_norm_initial = a.get_local_part();
        let lowercase = local_norm_initial.to_lowercase();
        // todo ... more normalization rules
        let local_norm = match domain_norm.as_ref() {
            "gmail.com" | "googlemail.com" => lowercase.split("+").next().unwrap_or(&lowercase),
            _ => local_norm_initial,
        };
        Self(format!("{}@{}", domain_norm, local_norm))
    }
}

#[derive(Debug, Copy, Clone)]
pub struct InvalidEmail;

impl From<InvalidEmail> for Status {
    fn from(_: InvalidEmail) -> Self {
        Status::invalid_argument("malformed email")
    }
}

pub struct ParsedEmailAddress(EmailAddress);

impl ParsedEmailAddress {
    pub fn to_storable(&self) -> StoredEmailAddress {
        StoredEmailAddress {
            address: self.0.to_string(),
        }
    }
}

impl From<EmailAddress> for ParsedEmailAddress {
    fn from(e: EmailAddress) -> Self {
        Self(e)
    }
}

impl FromStr for ParsedEmailAddress {
    type Err = InvalidEmail;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        email_address_parser::EmailAddress::parse(s, Some(ParsingOptions::new(true)))
            .map(ParsedEmailAddress::from)
            .ok_or(InvalidEmail)
    }
}
