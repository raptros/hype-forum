use chrono::DateTime;
use derive_more::Display;
use diesel::RunQueryDsl;
use diesel::{ExpressionMethods, QueryDsl};
use diesel_derive_newtype::DieselNewType;
use tracing::instrument;

use hype_forum_protos::hype_forum::account::session_info::CallerInfo;
use hype_forum_protos::hype_forum::account::{SessionHash, SessionIndexData, SessionInfo};
use hype_forum_protos::hype_forum::{CursorDirection, SessionLoginType, SessionTokenInner};

use crate::data::users::UserId;
use crate::data::{DataError, HasPool, Limit, LimitDslExt};
use crate::message_crypt::SecureHashExt;
use crate::schema::sessions;
use crate::ServerContext;

#[derive(Copy, Clone, Hash, Debug, PartialEq, Eq, AsExpression, DieselNewType, Display)]
#[display(format = "s:{}", _0)]
pub struct SessionId(pub(in crate::data) i64);

#[derive(Queryable, Identifiable)]
#[table_name = "sessions"]
// note: Queryable uses field order, not field name. therefore essential to keep fields here in the same order as schema.
pub struct Session {
    pub id: SessionId,

    pub user_id: UserId,

    pub is_valid: bool,

    pub login_type: i32,
    pub ip: Option<String>,
    pub user_agent: Option<String>,

    pub expires_at: Option<DateTime<chrono::Utc>>,

    pub created_at: DateTime<chrono::Utc>,
}

#[derive(Insertable, Debug)]
#[table_name = "sessions"]
pub struct NewSession {
    pub user_id: UserId,
    pub is_valid: bool,
    pub login_type: i32,

    pub ip: Option<String>,
    pub user_agent: Option<String>,

    pub expires_at: Option<DateTime<chrono::Utc>>,
}

#[tonic::async_trait]
pub trait Sessions: HasPool {
    /// this is the basic database operation for retrieving a session by id
    #[instrument(skip(self))]
    async fn get_session(&self, session_id: SessionId) -> Result<Option<Session>, DataError> {
        use diesel::OptionalExtension;
        // whenever caching is added, this is where a cache attempt would occur (before going to blocking db call)
        self.spawn_blocking_with_conn(move |conn| {
            sessions::table
                .find(session_id)
                .get_result(conn)
                .optional()
                .map_err(|error| {
                    tracing::error!(?error, "failed to retrieve session");
                    DataError
                })
        })
        .await
    }

    /// this method creates a session and returns a session with an id
    #[instrument(skip(self))]
    async fn insert_session(&self, new_session: NewSession) -> Result<Session, DataError> {
        self.spawn_blocking_with_conn(move |conn| {
            diesel::insert_into(sessions::table)
                .values(new_session)
                .get_result(conn)
                .map_err(|error| {
                    tracing::error!(?error, "session insert failed");
                    DataError
                })
            // note: this is where caching would go
        })
        .await
    }

    /// retrieve a page of sessions from the database for the target user
    #[instrument(skip(self))]
    async fn list_sessions(
        &self,
        target_user_id: UserId,
        session_cursor: Option<SessionIndexData>,
        cursor_type: CursorDirection,
        limit: Limit,
    ) -> Result<Vec<Session>, DataError> {
        use crate::schema::sessions::dsl::*;
        self.spawn_blocking_with_conn(move |conn| {
            let mut query = sessions.filter(user_id.eq(target_user_id)).into_boxed();
            if let Some(data) = session_cursor {
                match cursor_type {
                    CursorDirection::None => (),
                    CursorDirection::StartingAfter => {
                        query = query.filter(id.gt(data.session_id));
                    }
                    CursorDirection::EndingBefore => {
                        query = query.filter(id.lt(data.session_id));
                    }
                }
            }
            query
                .order_by(id.desc())
                .limit_n(limit)
                .load(conn)
                .map_err(|error| {
                    tracing::error!(?error, "failed to load sessions");
                    DataError
                })
        })
        .await
    }

    #[instrument(skip(self))]
    async fn disable_sessions(&self, session_ids: Vec<SessionId>) -> Result<usize, DataError> {
        use crate::schema::sessions::dsl::*;
        // potentially multiple writes occur here
        self.spawn_transaction(move |conn| {
            let result =
                diesel::update(sessions.filter(id.eq(diesel::expression::dsl::any(session_ids))))
                    .set(is_valid.eq(false))
                    .execute(conn)?;
            Ok(result)
        })
        .await
    }
}

#[tonic::async_trait]
impl Sessions for ServerContext {}

impl From<Session> for SessionInfo {
    fn from(s: Session) -> Self {
        let hash = s
            .get_index_data()
            .secure_hash()
            .ok()
            .map(|data| SessionHash { data });

        Self {
            hash,
            current_user_id: s.user_id.0,
            created_at: Some(s.created_at.into()),
            expires_at: s.expires_at.map(|e| e.into()),
            created_by: Some(CallerInfo {
                ip_info: s.ip.unwrap_or_else(|| "".into()),
                user_agent: s.user_agent.unwrap_or_else(|| "".into()),
            }),
        }
    }
}

impl Session {
    pub fn get_index_data(&self) -> SessionIndexData {
        SessionIndexData {
            session_id: self.id.0,
            user_id: self.user_id.0,
            // note the clone and into
            created_at: Some(self.created_at.into()),
        }
    }

    pub fn get_session_token(&self) -> SessionTokenInner {
        SessionTokenInner {
            session_id: self.id.0,
            user_id: self.user_id.0,
            expires_at: self.expires_at.map(|dt| dt.into()),
            login_type: self.login_type,
        }
    }
}

/// represents a "live" session token - ie the contents of SessionTokenInner mapped into useful
/// internal representations. for instance, ids mapped into their newtypes, login type in enum rep,
/// expires_at mapped into DateTime.
/// there's a From<SessionTokenInner> instance (since it's infallibly mappable),
/// and there's a PartialEq<Session> instance for it, useful for validating tokens against the database.
#[derive(Debug, Clone, PartialEq)]
pub struct LiveSessionToken {
    pub session_id: SessionId,
    pub user_id: UserId,
    pub login_type: SessionLoginType,
    pub expires_at: Option<DateTime<chrono::Utc>>,
}

impl PartialEq<Session> for LiveSessionToken {
    #[allow(clippy::suspicious_operation_groupings)]
    fn eq(&self, other: &Session) -> bool {
        // "other" is not a LiveSessionToken, but a Session object, so it has an `id` field instead of a `session_id` field
        self.session_id == other.id
            && self.user_id == other.user_id
            && self.login_type as i32 == other.login_type
            && self.expires_at == other.expires_at
    }
}

impl From<SessionTokenInner> for LiveSessionToken {
    fn from(token: SessionTokenInner) -> Self {
        Self {
            session_id: SessionId(token.session_id),
            user_id: UserId(token.user_id),
            // order matters here for te borrow checker
            login_type: token.login_type(),
            expires_at: token.expires_at.map(|ts| ts.into()),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct LiveSessionIndex {
    pub session_id: SessionId,
    pub user_id: UserId,
    pub created_at: Option<chrono::DateTime<chrono::Utc>>,
}

impl From<SessionIndexData> for LiveSessionIndex {
    fn from(data: SessionIndexData) -> Self {
        Self {
            session_id: SessionId(data.session_id),
            user_id: UserId(data.user_id),
            created_at: data.created_at.map(chrono::DateTime::from),
        }
    }
}
