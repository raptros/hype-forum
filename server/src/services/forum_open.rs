use tonic::{Request, Response, Status};
use tracing::instrument;

use hype_forum::LoginValue;
use hype_forum_protos::hype_forum;
use hype_forum_protos::hype_forum::login_value::Method;
use hype_forum_protos::hype_forum::{
    LoginResponse, RegisterResponse, Registration, SessionLoginType, SessionToken,
    StoredEmailAddress,
};

use crate::data::sessions::{NewSession, Sessions};
use crate::data::users::accounts::{Accounts, LoadedUserAccount};
use crate::data::users::{NewUser, User, Users};
use crate::data::wrappers::EmailHash;
use crate::{EmailCrypt, ServerContext, SessionCrypt};
use argon2::{password_hash::SaltString, Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use hype_forum_protos::hype_forum::forum_open_server::ForumOpenServer;
use std::convert::TryFrom;
use std::net::SocketAddr;

pub struct ForumOpenImpl {
    context: ServerContext,
}

#[tonic::async_trait]
impl hype_forum::forum_open_server::ForumOpen for ForumOpenImpl {
    #[instrument(skip(self, request), err)]
    async fn login(&self, request: Request<LoginValue>) -> Result<Response<LoginResponse>, Status> {
        let user_agent = request
            .metadata()
            .get("user-agent")
            .and_then(|d| d.to_str().ok())
            .map(|s| s.to_owned());

        let caller_ip = request.remote_addr();
        let method = request
            .get_ref()
            .method
            .as_ref()
            .ok_or_else(|| Status::invalid_argument("must provide a login method"))?;

        let user = match method {
            Method::Guest(_) => {
                // todo: check if server permits guest logins
                let new_user = NewUser {
                    // todo: configuration
                    display_name: "Guest".into(),
                };

                // todo: note that if session token generation fails below, this leaves a stranded user record
                //       only way that can be fixed is by using transactions
                self.context.insert_user(new_user).await?
            }

            Method::UserPass(user_pass) => {
                let user_tag = user_pass.user.as_str();
                let password = user_pass.password.as_str();
                let user = self
                    .context
                    // note: this clones the user tag string. needed for sql
                    .load_account(user_tag.into())
                    .await?
                    .ok_or_else(|| {
                        tracing::warn!(target_user = user_tag, "missing login");
                        Status::unauthenticated("bad credentials")
                    })?;

                verify_password(&user, password).await?;
                user.user
            }
        };

        let login_response = self
            .create_session(SessionLoginType::Guest, user, user_agent, caller_ip)
            .await?;

        Ok(Response::new(login_response))
    }

    #[instrument(skip(self, request), err)]
    async fn register(
        &self,
        request: Request<Registration>,
    ) -> Result<Response<RegisterResponse>, Status> {
        // todo: need to check configuration to see if registration is even open
        // todo: also need to provide some more advanced functionality - eg invite codes
        let Registration {
            user_name,
            email_address,
            password,
        } = request.into_inner();

        // validate the username
        self.validate_username(user_name.as_str())?;

        let salt = SaltString::generate(rand::thread_rng());
        let argon = Argon2::default();
        // todo: some kind of length validation (keep it below like 100 bytes or something)
        let pw_hash = argon
            .hash_password_simple(password.as_bytes(), salt.as_ref())
            .map_err(|error| {
                tracing::info!(?error, "someone tried to register with an invalid password");
                Status::invalid_argument("password")
            })?
            .to_string();

        // todo: nonce!
        let email_hash = EmailHash::parsed(&email_address, "yeet")?;

        let address = StoredEmailAddress {
            address: email_address,
        };
        let encrypted_address = self
            .context
            .get_email_crypt()
            .encrypt_message(&address)
            .map_err(|_| {
                tracing::error!("failed to encrypt email address!");
                Status::invalid_argument("email")
            })?;

        let user = self
            .context
            .register(user_name, pw_hash, email_hash, encrypted_address)
            .await?;
        Ok(Response::new(RegisterResponse { user_id: user.id.0 }))
    }
}

impl ForumOpenImpl {
    pub fn new(context: ServerContext) -> Self {
        ForumOpenImpl { context }
    }

    pub fn new_server(context: ServerContext) -> ForumOpenServer<Self> {
        ForumOpenServer::new(Self::new(context))
    }

    #[instrument(skip(self))]
    async fn create_session(
        &self,
        login_type: SessionLoginType,
        user: User,
        user_agent: Option<String>,
        caller_ip: Option<SocketAddr>,
    ) -> Result<LoginResponse, Status> {
        // todo: actually compute expiration based on config
        let expiration = None;

        // todo: encrypt PII fields!
        let new_session = NewSession {
            user_id: user.id,
            login_type: login_type.into(),
            is_valid: true,
            expires_at: expiration,
            ip: caller_ip.map(|ip| ip.to_string()),
            user_agent,
        };

        let new_session = self.context.insert_session(new_session).await?;

        let token_data = new_session.get_session_token();

        let encrypted_session = self
            .context
            .get_session_crypt()
            .encrypt_message(&token_data)
            .map_err(|_| {
                tracing::error!(
                    session = ?new_session.id,
                    user = ?new_session.user_id,
                    ?login_type,
                    ?expiration,
                    "failed to encrypt session token!",
                );
                Status::internal("server error")
            })?;

        let token = SessionToken {
            data: encrypted_session,
        };
        Ok(LoginResponse {
            session_token: Some(token),
            expires_at: token_data.expires_at,
        })
    }

    /// method for checking that a username is allowed for registration
    /// todo: add various features
    fn validate_username(&self, user_name: &str) -> Result<(), Status> {
        // never allow usernames to contain newlines lol
        if user_name.contains('\n') {
            return Err(Status::invalid_argument("username"))
        }
        Ok(())
    }
}

/// this function does some heavy computation - it performs argon2 password hashing.
#[instrument(skip(loaded_user_account, submitted_password))]
async fn verify_password(
    loaded_user_account: &LoadedUserAccount,
    submitted_password: &str,
) -> Result<(), Status> {
    let LoadedUserAccount {
        user,
        account: _,
        password,
        emails: _,
    } = loaded_user_account;
    // todo
    let pw_hash = password.hash.as_str();
    let hash = PasswordHash::try_from(pw_hash).map_err(|error| {
        tracing::error!(user = %user.id, ?error, "invalid password hash");
        Status::internal("something went wrong")
    })?;
    // todo: run all the password verification operation inside a fixed size threadpool. see issue #2.
    tokio::task::block_in_place(|| {
        let argon = Argon2::default();
        // the signature of verify_password in argon is the silliest danged thing.
        // the good news though is that just about any failure implies that the submitted password is incorrect.
        argon
            .verify_password(submitted_password.as_bytes(), &hash)
            .map_err(|error| {
                tracing::warn!(user = %user.id, ?error, "bad password attempt");
                // todo: unified response strings
                Status::unauthenticated("bad credentials")
            })
    })
}
