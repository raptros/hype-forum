use itertools::Itertools;
use tonic::{Request, Response, Status};

use hype_forum_protos::hype_forum::threads::thread_server::ThreadServer;
use hype_forum_protos::hype_forum::threads::{
    EditPostRequest, GetPostsRequest, GetThreadInfoRequest, PostThreadRequest, PostThreadResponse,
    ReplyRequest,
};
use hype_forum_protos::hype_forum::threads::{
    EditPostResponse, GetPostsResponse, GetThreadInfoResponse, ReplyResponse,
};

use crate::data::boards::BoardId;
use crate::data::threads::{NewPost, NewThread, ThreadId, ThreadPostCursor, ThreadsPosts};

use crate::services::AuthManager;
use crate::ServerContext;

pub struct ThreadImpl {
    context: ServerContext,
}

impl ThreadImpl {
    pub fn new(context: ServerContext) -> Self {
        Self { context }
    }

    pub fn new_server(context: ServerContext) -> ThreadServer<Self> {
        ThreadServer::new(Self::new(context))
    }
}

#[tonic::async_trait]
impl hype_forum_protos::hype_forum::threads::thread_server::Thread for ThreadImpl {
    async fn post(
        &self,
        request: Request<PostThreadRequest>,
    ) -> Result<Response<PostThreadResponse>, Status> {
        let (auth, mut request) = self.context.auth(request).await?;
        // auth is responsible for determining that the board even exists, let alone that the user can post to it

        let post_text = request
            .first_post
            .take()
            .ok_or_else(|| Status::invalid_argument("need post body"))?
            .post_text;

        let new_thread = NewThread {
            board_id: BoardId(request.board_id),
            poster_id: auth.token.user_id,
            // todo: validate the thread title
            display_title: request.thread_title,
            // todo: validation (and trimming in fallback case?)
            about: request.thread_about.unwrap_or_else(|| post_text.clone()),
        };

        let (thread, post) = self.context.post_thread(new_thread, post_text).await?;

        let response = PostThreadResponse {
            new_thread: Some(thread.into()),
            first_post: Some(post.into()),
        };

        Ok(Response::new(response))
    }

    async fn get_thread_info(
        &self,
        request: Request<GetThreadInfoRequest>,
    ) -> Result<Response<GetThreadInfoResponse>, Status> {
        let (_auth, request) = self.context.auth(request).await?;
        // eventually we'll use the auth object to update user activity stats.
        // todo: thread info request object?
        let thread_id = ThreadId(request.thread_id);
        let thread = self
            .context
            .get_thread(thread_id)
            .await?
            .ok_or_else(|| Status::not_found("thread not found"))?;

        let response = GetThreadInfoResponse {
            info: Some(thread.into()),
            // todo: implement retrieving posting banners.
            posting_banners: None,
        };
        Ok(Response::new(response))
    }

    async fn get_posts(
        &self,
        request: Request<GetPostsRequest>,
    ) -> Result<Response<GetPostsResponse>, Status> {
        let (_auth, request) = self.context.auth(request).await?;
        let cursor = request
            .cursor
            .map(ThreadPostCursor::from)
            .unwrap_or_default();
        let posts = self
            .context
            .list_posts(ThreadId(request.thread_id), cursor)
            .await?;
        let posts = posts.into_iter().map(|p| p.into()).collect_vec();
        let response = GetPostsResponse { posts };
        Ok(Response::new(response))
    }

    async fn reply(
        &self,
        request: Request<ReplyRequest>,
    ) -> Result<Response<ReplyResponse>, Status> {
        let (auth, mut request) = self.context.auth(request).await?;
        // todo: thread lock status etc
        let post_text = request
            .reply_body
            .take()
            .ok_or_else(|| Status::invalid_argument("post body required"))?
            .post_text;

        // todo: metadata (reply info etc).
        let new_post = NewPost {
            thread_id: ThreadId(request.thread_id),
            user_id: auth.token.user_id,
            post_text,
        };

        // todo: figure out how we're going to deal with metadata issues
        let result = self
            .context
            .insert_post(new_post)
            .await?
            .ok_or_else(|| Status::not_found("no such post"))?;

        let response = ReplyResponse {
            post: Some(result.into()),
        };
        Ok(Response::new(response))
    }

    async fn edit_post(
        &self,
        request: Request<EditPostRequest>,
    ) -> Result<Response<EditPostResponse>, Status> {
        let (_auth, _request) = self.context.auth(request).await?;
        todo!()
    }
}
