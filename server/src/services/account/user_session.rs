use itertools::Itertools;
use sha3::Digest;
use tonic::metadata::MetadataValue;
use tonic::{Request, Response, Status};
use tracing::instrument;

use hype_forum_protos::hype_forum;
use hype_forum_protos::hype_forum::account::list_sessions_response::SessionListItem;
use hype_forum_protos::hype_forum::account::DisableSessionsRequest;
use hype_forum_protos::hype_forum::account::GetSessionInfoRequest;
use hype_forum_protos::hype_forum::account::ListSessionsRequest;
use hype_forum_protos::hype_forum::account::{
    DisableSessionsResponse, GetSessionInfoResponse, ListSessionsResponse, SessionHash,
    SessionIndex, SessionIndexData,
};
use hype_forum_protos::hype_forum::{CursorDirection, PermitRiskyAction};

use crate::data::sessions::{LiveSessionIndex, Sessions};
use crate::data::users::Users;
use crate::data::Limit;
use crate::generic_utils::ResultFilterExt;
use crate::message_crypt::SecureHashExt;
use crate::services::AuthManager;
use crate::ServerContext;
use hype_forum_protos::hype_forum::account::user_session_server::UserSessionServer;

pub struct UserSessionImpl {
    context: ServerContext,
}

impl UserSessionImpl {
    pub fn new(context: ServerContext) -> Self {
        UserSessionImpl { context }
    }

    pub fn new_server(context: ServerContext) -> UserSessionServer<Self> {
        UserSessionServer::new(Self::new(context))
    }
}

#[tonic::async_trait]
impl hype_forum::account::user_session_server::UserSession for UserSessionImpl {
    #[instrument(skip(self), err)]
    async fn get_session_info(
        &self,
        request: Request<GetSessionInfoRequest>,
    ) -> Result<Response<GetSessionInfoResponse>, Status> {
        let (auth, request) = self.context.auth(request).await?;

        let user_id = auth.token.user_id;
        let user_info_future = async {
            if request.include_user_info {
                self.context
                    .get_user(user_id)
                    .await?
                    .ok_or_else(|| {
                        tracing::error!(user = %user_id, "authenticated token without user");
                        Status::internal("server error")
                    })
                    .map(Some)
            } else {
                Ok(None)
            }
        };

        let response = GetSessionInfoResponse {
            session_info: Some(auth.session.into()),
            user_info: user_info_future.await?.map(|u| u.into()),
        };
        Ok(Response::new(response))
    }

    #[instrument(skip(self), err)]
    async fn list_sessions(
        &self,
        request: Request<ListSessionsRequest>,
    ) -> Result<Response<ListSessionsResponse>, Status> {
        let (auth, request) = self.context.auth(request).await?;

        let cursor_type: CursorDirection = request.cursor_type();
        let limit: Limit = request.limit.into();

        let session_cursor = request
            .cursor_value
            .map(|c| {
                self.context
                    .session_crypt
                    .decrypt_message::<SessionIndexData>(c.data)
            })
            .transpose()
            .map_err(|_| Status::invalid_argument("bad cursor value"))?;

        let db_result = self
            .context
            .list_sessions(auth.token.user_id, session_cursor, cursor_type, limit)
            .await?;

        let mut hasher = sha3::Sha3_256::new();
        let result = db_result
            .into_iter()
            .map(|ses| {
                let index = ses.get_index_data();
                // we probably do not need an additional nonce here, as the object being hashed has a number
                // of not extremely guessable values, and the hash itself does not actually allow indexing into the database
                // in fact the hash is constructed so the client can match sessions between requests.
                let hash = index.secure_hash_reset(&mut hasher).map_err(|error| {
                    tracing::error!(?error, ?index, "failed to hash session index token");
                    Status::internal("server error")
                })?;
                let token =
                    self.context
                        .session_crypt
                        .encrypt_message(&index)
                        .map_err(|error| {
                            tracing::error!(
                                ?error,
                                ?index,
                                "failed to encrypt session index token"
                            );
                            Status::internal("server error")
                        })?;
                Ok(SessionListItem {
                    hash: Some(SessionHash { data: hash }),
                    index: Some(SessionIndex { data: token }),
                    current_session: ses.id == auth.token.session_id,
                    info: Some(ses.into()),
                })
            })
            .try_collect::<SessionListItem, Vec<SessionListItem>, Status>()?;

        Ok(Response::new(ListSessionsResponse { sessions: result }))
    }

    async fn disable_sessions(
        &self,
        request: Request<DisableSessionsRequest>,
    ) -> Result<Response<DisableSessionsResponse>, Status> {
        let (auth, request) = self.context.auth(request).await?;
        let target_sessions = request.target_sessions;

        let user_id = auth.token.user_id;
        let session_id = auth.token.session_id;
        let can_disable_current_session =
            request.permit_disable_current_session == PermitRiskyAction::Yes as i32;

        let (failed_indexes, target_session_ids): (Vec<_>, Vec<_>) = target_sessions
            .into_iter()
            .enumerate()
            .partition_map(|(idx, ses)| {
                self.context
                    .session_crypt
                    .decrypt_message::<SessionIndexData>(ses.data)
                    // CryptErr is meaningless - just get the indexes of failures
                    .map_err(|_| idx)
                    .map(LiveSessionIndex::from)
                    .require_or_else(|| idx, |s| s.user_id == user_id)
                    .require_or_else(
                        || idx,
                        |s| can_disable_current_session || s.session_id != session_id,
                    )
                    .map(|t| t.session_id)
                    .into()
            });

        if !failed_indexes.is_empty() {
            let mut status = Status::invalid_argument("bad indexes");
            let metadata = status.metadata_mut();
            for failed in failed_indexes {
                metadata.append("bad-index", MetadataValue::from(failed));
            }
            return Err(status);
        }

        let requested = target_session_ids.len();
        let updated = self.context.disable_sessions(target_session_ids).await?;
        tracing::info!(requested, updated, %user_id, "disabled sessions");

        Ok(Response::new(DisableSessionsResponse {}))
    }
}
