use chrono::{DateTime, Utc};
use hype_forum_protos::TakeSessionToken;
use tonic::{Request, Status};
use tracing::instrument;

use hype_forum_protos::hype_forum::{SessionToken, SessionTokenInner};

use crate::data::sessions::{LiveSessionToken, Session, Sessions};
use crate::{ServerContext, SessionCrypt};

// directory modules are for proto packages, file modules are for proto service impls.
// stay organized!

pub mod account;
pub mod browse;
pub mod forum_open;
pub mod threads;

pub struct Auth {
    pub token: LiveSessionToken,

    pub session: Session,
}

#[tonic::async_trait]
trait AuthManager: SessionCrypt + Sessions {
    /// performs authentication and authorization - i.e. given a SessionToken from a request,
    /// determines if the sesion token is valid, looks up the session, and grabs relevant user permissions.
    ///
    /// returns Err(Status) if any part of authN or authZ fails - that status should be returned immediately.
    ///
    /// todo: design authZ
    #[instrument(skip(self, request))]
    async fn auth<R: TakeSessionToken + Send>(
        &self,
        request: Request<R>,
    ) -> Result<(Auth, R), Status> {
        let mut message = request.into_inner();
        let token = message.take_session_token();
        let token = self.auth_token(token).await?;
        let session = self.check_session(&token).await?;
        let auth = Auth { token, session };
        Ok((auth, message))
    }

    // todo: does this function actually need to be async?
    #[instrument(skip(self, token_wrapper))]
    async fn auth_token(
        &self,
        token_wrapper: Option<SessionToken>,
    ) -> Result<LiveSessionToken, Status> {
        let token_data = token_wrapper
            .ok_or_else(|| Status::unauthenticated("missing token"))?
            .data;

        let token = self
            .get_session_crypt()
            .decrypt_message::<SessionTokenInner>(token_data)
            .map_err(|_| Status::unauthenticated("bad token"))?;

        // todo: session activity logging. see issue #3

        // check token itself
        if let Some(expires_at) = token.expires_at.as_ref() {
            let ts_actual: DateTime<Utc> = expires_at.clone().into();
            let current_time = Utc::now();
            if current_time > ts_actual {
                // todo: manage session expiration
                return Err(Status::unauthenticated("session expired"));
            }
        }

        Ok(token.into())
    }

    #[instrument(skip(self, token))]
    async fn check_session(&self, token: &LiveSessionToken) -> Result<Session, Status> {
        let session = self
            .get_session(token.session_id)
            .await
            .map_err(|_| Status::internal("database error"))?
            .ok_or_else(|| {
                // this can happen - if eg the session table needs to be truncated.
                tracing::info!(session = ?token.session_id, "missing session");
                // technically it's an internal error, but this message should tell the client to try logging in again
                Status::unauthenticated("bad token")
            })?;
        if !session.is_valid {
            return Err(Status::unauthenticated("session expired"));
        }
        if *token != session {
            // there are several ways this could happen - some possibilities:
            // 1. someone directly modifying a session record in the database (bad)
            // 2. someone has a token that was generated before a database truncation (bad) -
            //    this is why you should regenerate the session key after a session table truncation.
            // 3. someone is producing tokens of their own, and this server is basically pwned.
            tracing::error!(?token, "token does not match database!");
            // todo: invalidate session
            return Err(Status::unauthenticated("session expired"));
        }
        // we don't actually have to check expires_at - it has to match what was in the token, which should already have been checked.
        Ok(session)
    }
}

#[tonic::async_trait]
impl AuthManager for ServerContext {}
