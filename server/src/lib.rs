// lib.rs is separated out in order to support integration testing.
#[macro_use]
extern crate diesel;

use std::convert::TryFrom;
use std::sync::Arc;

use anyhow::Context;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use tonic::transport::Server;
use tracing::{info, instrument};

use crate::config::ServerConfig;
use crate::message_crypt::MessageCrypt;

pub mod config;
pub mod data;
pub mod generic_utils;
pub mod message_crypt;
pub mod schema;
pub mod services;

#[instrument(skip(context))]
pub async fn run(context: ServerContext) -> Result<(), Box<dyn std::error::Error>> {
    let reflect = tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(hype_forum_protos::hype_forum::FILE_DESCRIPTOR_SET)
        .build()?;

    info!(
        "starting server listening on {}",
        context.get_config().address
    );
    // todo: configure tls?
    Server::builder()
        .trace_fn(|_| tracing::info_span!("hype_forum_server"))
        .add_service(services::forum_open::ForumOpenImpl::new_server(
            context.clone(),
        ))
        .add_service(services::account::user_session::UserSessionImpl::new_server(context.clone()))
        .add_service(services::threads::ThreadImpl::new_server(context.clone()))
        .add_service(reflect)
        .serve(context.get_config().address)
        .await?;
    Ok(())
}

// not exactly sure how to structure this ... maybe implement clone? really depends on what it takes to add diesel pool.
#[derive(Clone)]
pub struct ServerContext {
    // no one should be trying to mutate config, so this is just Arc for lightweight cloning
    pub config: Arc<ServerConfig>,

    // lol
    // todo: configure for tracing
    pub pool: Pool<ConnectionManager<PgConnection>>,

    pub session_crypt: MessageCrypt,

    pub email_crypt: MessageCrypt,
}

// this handles setting up the server context from the config.
// it should set up database connections, thread pools, caches, etc
// in order to make ServerContext work
impl TryFrom<ServerConfig> for ServerContext {
    type Error = anyhow::Error;

    fn try_from(config: ServerConfig) -> Result<Self, Self::Error> {
        let manager = ConnectionManager::<PgConnection>::new(&config.database_url);
        let pool = Pool::builder()
            .build(manager)
            .with_context(|| "could not connect")?;

        let session_crypt = config.get_session_crypt()?;

        let email_crypt = config.get_email_crypt()?;

        let context = ServerContext {
            config: Arc::new(config),
            pool,
            session_crypt,
            email_crypt,
        };

        Ok(context)
    }
}

// this should provide a useful interface for working with the server's backing stores etc across threads.
impl ServerContext {
    pub fn get_config(&self) -> &ServerConfig {
        &self.config
    }
}

pub trait SessionCrypt {
    fn get_session_crypt(&self) -> &MessageCrypt;
}

impl SessionCrypt for ServerContext {
    fn get_session_crypt(&self) -> &MessageCrypt {
        &self.session_crypt
    }
}

pub trait EmailCrypt {
    fn get_email_crypt(&self) -> &MessageCrypt;
}

impl EmailCrypt for ServerContext {
    fn get_email_crypt(&self) -> &MessageCrypt {
        &self.email_crypt
    }
}
