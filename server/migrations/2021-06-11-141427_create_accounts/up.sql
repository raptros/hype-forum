-- this table represents the account info of a registered (i.e. non-guest) user.
create table user_accounts (
    -- only one account record per user
    user_id    bigint primary key references users,

    -- user_slug is a representation of the user's name that is sufficiently normalized to be useful for querying
    user_slug  varchar     not null unique,

    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on user_accounts
    for each row
execute procedure trigger_set_timestamp();

create table account_emails (
    -- use a normalized email hash for this
    -- it has to be queryable, so it can't have a nonce inside it - that'll need to be supplied by the server.
    -- there's not really a way to work around the fact that a hashed email is a byte array,
    -- and we need to index this field in order to use it for login.
    email_hash      bytea primary key,
    -- users can have multiple emails
    user_id         bigint      not null references users,

    -- todo: representation of this
    -- never store email addresses in plaintext lol
    encrypted_email bytea       not null,

    -- todo: figure out where notification settings should be stored

    created_at      timestamptz not null default now(),
    updated_at      timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on account_emails
    for each row
execute procedure trigger_set_timestamp();

-- passwords in a separate table for safety.
create table account_passwords (
    -- only one password per user
    user_id    bigint primary key references users,
    hash       varchar     not null,
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on account_passwords
    for each row
execute procedure trigger_set_timestamp();
