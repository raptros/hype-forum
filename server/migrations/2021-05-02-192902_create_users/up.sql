create or replace function trigger_set_timestamp()
    returns trigger as
$$
begin
    -- note that this means that any table using this function has to make the update column use this name.
    new.updated_at = now();
    return new;
end;
$$ language plpgsql;

-- this table is for top level public user information.
create table users (
    id           bigserial primary key,
    display_name varchar     not null,
    created_at   timestamptz not null default now(),
    updated_at   timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on users
    for each row
execute procedure trigger_set_timestamp();
