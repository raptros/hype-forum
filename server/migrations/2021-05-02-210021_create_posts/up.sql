create table posts (
    id         bigserial primary key,
    thread_id  bigint      not null references threads,
    user_id    bigint      not null references users,

    post_text  varchar     not null,

    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);
create trigger set_timestamp
    before update
    on posts
    for each row
execute procedure trigger_set_timestamp();
