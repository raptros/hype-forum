create table threads (
    id            bigserial primary key,

    board_id      bigint      not null references boards,

    poster_id     bigint      not null references users,

    display_title varchar     not null,

    about         varchar     not null,

    created_at    timestamptz not null default now(),

    updated_at    timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on threads
    for each row
execute procedure trigger_set_timestamp();
