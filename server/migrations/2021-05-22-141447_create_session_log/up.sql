create table session_log (
    -- ok
    id         bigserial primary key,
    session_id bigint      not null references sessions,

    -- info about the most recent use of the session
    -- this should be handled in the background
    ip         varchar     null,
    -- todo: set a limit on the size of this column
    user_agent varchar     null,

    service    varchar     not null,
    method     varchar     not null,

    created_at timestamptz not null default now()
);
