create table sessions (
    -- ok
    id         bigserial primary key,
    user_id    bigint      not null references users,
    -- when the server discovers a session has reached the expiration date,
    -- it should set this to false. also, users should be able to clear out active sessions themselves.
    is_valid   boolean     not null default true,

    -- this is interpreted by the SessionLoginType enum in the proto
    login_type integer     not null,

    -- todo: encrypt these PII fields!
    -- the ip and user agent used to create the session
    ip         varchar     null,
    -- todo: set a limit on the size of this column
    user_agent varchar     null,

    -- this can be null because a forum may not want sessions ever expiring
    expires_at timestamptz null,

    created_at timestamptz not null default now()
);

