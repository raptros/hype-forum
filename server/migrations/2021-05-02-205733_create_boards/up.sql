create table boards (
    id           bigserial primary key,

    parent_id    bigint      null references boards,

    display_name varchar,

    banner       varchar,

    -- this is when the board was created
    created_at   timestamptz not null default now(),

    updated_at   timestamptz not null default now()
);

create trigger set_timestamp
    before update
    on boards
    for each row
execute procedure trigger_set_timestamp();
