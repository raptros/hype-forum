use darling::FromDeriveInput;
use darling::FromField;
use proc_macro2::{Span, TokenStream};
use proc_macro_crate::FoundCrate;
use quote::ToTokens;
use syn::{parse_macro_input, DeriveInput, GenericArgument, Ident, PathArguments, Type};

#[proc_macro_derive(TakeSessionToken, attributes(take_session_token))]
pub fn derive_has_session_token(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let parsed = match TakeSessionTokenReceiver::from_derive_input(&input) {
        Ok(p) => p,
        Err(e) => return e.write_errors().into(),
    };
    let expanded = parsed.into_token_stream();
    expanded.into()
}

#[derive(Debug, FromDeriveInput)]
#[darling(attributes(take_session_token))]
struct TakeSessionTokenReceiver {
    ident: Ident,
    data: darling::ast::Data<(), StructFieldReceiver>,
    #[darling(default)]
    no_fail: bool,
    #[darling(default)]
    fail_on_suffix: Option<String>,
}

impl ToTokens for TakeSessionTokenReceiver {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let no_fail = match self.fail_on_suffix.as_ref() {
            None => self.no_fail,
            Some(suffix) => self.no_fail && !self.ident.to_string().ends_with(suffix),
        };

        let found_crate = proc_macro_crate::crate_name("hype-forum-protos")
            .expect("must have hype-forum-protos in `Cargo.toml`");
        let crate_ident = match found_crate {
            FoundCrate::Itself => Ident::new("crate", Span::call_site()),
            FoundCrate::Name(name) => Ident::new(&name, Span::call_site()),
        };

        let struct_op = self.data.as_ref().take_struct();
        let fields = match struct_op {
            None => {
                if no_fail {
                    return;
                } else {
                    let err = darling::error::Error::unsupported_shape("enums are not supported")
                        .write_errors();
                    tokens.extend(err);
                    return;
                }
            }
            Some(s) => s.fields,
        };

        let self_name = &self.ident;

        let target_field_opt = fields.iter().find(|f| ty_is_token_field(&f.ty));

        let target_field = match target_field_opt {
            None => {
                if no_fail {
                    return;
                } else {
                    let err = darling::error::Error::unsupported_shape(
                        "does not have Option<SessionToken> field",
                    )
                    .write_errors();
                    tokens.extend(err);
                    return;
                }
            }
            Some(f) => f,
        };

        let field_name = match target_field.ident.as_ref() {
            None => {
                if no_fail {
                    return;
                } else {
                    let err = darling::error::Error::unsupported_shape("fields without names!")
                        .write_errors();
                    tokens.extend(err);
                    return;
                }
            }
            Some(i) => i,
        };

        let trait_impl = quote::quote! {
            impl #crate_ident::TakeSessionToken for #self_name {
                fn take_session_token(&mut self) -> Option<#crate_ident::hype_forum::SessionToken> {
                    self.#field_name.take()
                }
            }
        };

        tokens.extend(trait_impl);
    }
}

fn ty_is_token_field(ty: &Type) -> bool {
    let pt = match ty {
        Type::Path(pt) => pt,
        _ => return false,
    };

    if pt.qself.is_some() {
        return false;
    }

    let seg = match pt.path.segments.iter().find(|s| s.ident == "Option") {
        None => return false,
        Some(s) => s,
    };

    let args = match &seg.arguments {
        PathArguments::AngleBracketed(angle) => &angle.args,
        _ => return false,
    };

    if args.len() != 1 {
        return false;
    }
    let first_arg = match args.iter().next().as_ref() {
        Some(GenericArgument::Type(Type::Path(pt))) => pt,
        _ => return false,
    };
    if first_arg.qself.is_some() {
        return false;
    }

    let last_arg_seg = match first_arg.path.segments.last() {
        Some(seg) => seg,
        None => return false,
    };

    last_arg_seg.ident == "SessionToken"
}

#[derive(Debug, FromField)]
struct StructFieldReceiver {
    ident: Option<Ident>,
    ty: Type,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
