hype-forum-derives
==================

this provides derive macros


TakeSessionToken
----------------

automatically implements the `TakeSessionToken` trait,
which permits taking a session token out of any object which has a `SessionToken` field.

the derive macro is used by `hype-forum-protos` to enable easy auth of request messages. 

the macro supports an attribute `take_session_token` with a flag `no_fail` and a field `fail_on_suffix`. used together, 
they allow suppressing derivation failures except on things named a certain way. 
this is primarily used to enable automatic annotation of every proto struct while enforcing the presence of a session token.

example
```rust
#[derive(crate::TakeSessionToken)]
#[take_session_token(no_fail, fail_on_suffix = "Request")]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetSessionInfoRequest {
    #[prost(message, optional, tag = "1")]
    pub session_token: ::core::option::Option<super::SessionToken>,
    #[prost(bool, tag = "2")]
    pub include_user_info: bool,
}
```

if the field `session_token` was not present, then the derivation in that case would cause a compilation failure immediately.

without having `fail_on_suffix` set that wait, instead you would see a silent failure as on

```rust
#[derive(crate::TakeSessionToken)]
#[take_session_token(no_fail, fail_on_suffix = "Request")]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateBoardResponse {
    #[prost(int64, tag = "1")]
    pub id: i64,
}
```

which simply does not end up with an actual implementation of `TakeSessionToken`.