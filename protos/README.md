hf\_proto
=========

this crate collects the proto defs for hype-forum.

proto file organization
-----------------------
multiple proto files can be in a single package, and directory structure does not appear to particularly matter to
protoc.

the correspondence between proto package, rust mod, and directory name has to be manually constructed, which we will be
maintaining in this project.

the reason to impose this organization is so that rust source code to support any given proto package can live in the
same directory as the proto files it supports.
furthermore, good organization will make it easier to understand how everything fits together and where to find things.

module/package structure
------------------------
* `hype_forum` is the top level proto module, with login service etc
  * `account` for account management services - eg sessions, user preferences, etc
  * `admin` administration functions
  * `browse` services and their methods for browsing the forum - e.g. looking through the board hierarchy
  * `threads` services and types for interacting with threads and posts

using with intellij
-------------------

2021-05-14: if you're using intellij, right now enable the Experimental Features:
* org.rust.cargo.evaluate.build.scripts
* org.rust.macros.proc
* org.rust.resolve.new.engine

these will make autocompletion and jump-to-source work.
this requires us to only use one layer of macros for importing, which is why we will not use tonic's import macro.

clippy notes
------------

* if you ever see a `clippy::match_single_binding` warning, and it looks like it's from generated code, this probably
  means there's a service sitting around with no methods. 
  don't try to suppress this warning for generated code, instead make sure services all have methods defined on them.