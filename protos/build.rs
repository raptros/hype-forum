fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    tonic_build::configure()
        // this fail_on_suffix business is here so that anything message named *Request gets early enforcement of having a session token field
        // the TakeSessionToken derive macro will otherwise silently fail and not provide an implementation for the trait.
        // if you look at the generated code you'll see that the
        .type_attribute(".", "#[derive(crate::TakeSessionToken)] #[take_session_token(no_fail, fail_on_suffix = \"Request\")]")
        .file_descriptor_set_path(out_dir.join("hype_forum_descriptor.bin"))
        .format(true)
        .compile(&["src/forum.proto"], &["src/"])?;
    Ok(())
}
