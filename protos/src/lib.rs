pub use hype_forum_derives::*;

pub trait TakeSessionToken {
    fn take_session_token(&mut self) -> Option<hype_forum::SessionToken>;
}

pub mod hype_forum {
    use chrono::{DateTime, TimeZone, Utc};
    // we're using include! instead of tonic's include because it makes it more clear what's going on - in particular, to intellij
    include!(concat!(env!("OUT_DIR"), "/hype_forum.rs"));

    pub const FILE_DESCRIPTOR_SET: &[u8] =
        tonic::include_file_descriptor_set!("hype_forum_descriptor");

    impl From<Timestamp> for DateTime<Utc> {
        fn from(t: Timestamp) -> Self {
            Utc.timestamp(t.seconds, t.nanos)
        }
    }

    impl<Tz: TimeZone> From<DateTime<Tz>> for Timestamp {
        fn from(dt: DateTime<Tz>) -> Self {
            let seconds = dt.timestamp();
            let nanos = dt.timestamp_subsec_nanos();
            Timestamp { seconds, nanos }
        }
    }

    impl<Tz: TimeZone> From<&DateTime<Tz>> for Timestamp {
        fn from(dt: &DateTime<Tz>) -> Self {
            let seconds = dt.timestamp();
            let nanos = dt.timestamp_subsec_nanos();
            Timestamp { seconds, nanos }
        }
    }
    pub mod account {
        include!(concat!(env!("OUT_DIR"), "/hype_forum.account.rs"));
    }
    pub mod admin {
        include!(concat!(env!("OUT_DIR"), "/hype_forum.admin.rs"));
    }
    pub mod browse {
        include!(concat!(env!("OUT_DIR"), "/hype_forum.browse.rs"));
    }
    pub mod threads {
        include!(concat!(env!("OUT_DIR"), "/hype_forum.threads.rs"));
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
