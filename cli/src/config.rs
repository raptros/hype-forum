// use anyhow::{Result, Error};
use structopt::StructOpt;
// use std::fs::File;

// const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const PROGRAM_NAME: &'static str = env!("CARGO_PKG_NAME");

#[derive(Debug)]
pub struct Config {
    /// Server to connect to
    pub server: String,

    pub config_dir: std::path::PathBuf,
    pub data_dir: std::path::PathBuf,
}

impl Config {
    // ~/.config/hyphy/style.toml
    pub fn get_user_theme(&self) -> std::path::PathBuf {
        let config_dir = self.config_dir.clone();
        let theme_path = config_dir.join("style.toml");
        theme_path
    }

    pub fn initialize_theme(&self, siv: &mut cursive::CursiveRunnable) {
        let user_theme = self.get_user_theme();
        let theme_load_result = siv.load_theme_file(user_theme);
        match theme_load_result {
            Ok(_) => (),
            Err(error) => {
                debug!("Got an error opening user theme, defaulting to embedded theme, error was: {:?}", error);
                siv.load_toml(include_str!("../assets/style.toml"))
                    .expect("Failed to initialize embedded theme, this shouldn't happen!");
            }
        }
    }
}

pub fn get_config() -> Config {
    let opt: Opt = Opt::from_args();

    let config_dir = get_user_config_dir();
    let data_dir = get_user_data_dir();

    Config {
        server: opt.server,
        config_dir,
        data_dir,
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "hyphy", about = "A terminal client for hype forums")]
pub struct Opt {
    /// Server to connect to
    #[structopt(short = "s", long = "server")]
    pub server: String,
}

pub fn get_user_config_dir() -> std::path::PathBuf {
    let mut config_dir =
        dirs::config_dir().unwrap_or_else(|| std::path::PathBuf::from("~/.config"));
    config_dir.push(PROGRAM_NAME);
    std::fs::create_dir_all(&config_dir).expect("Failed to create hyphy's config directory");
    config_dir
}

// ~/.local/share/hyphy/hyphy.sqlite
pub fn get_user_data_dir() -> std::path::PathBuf {
    let mut data_dir =
        dirs::data_dir().unwrap_or_else(|| std::path::PathBuf::from("~/.local/share"));
    data_dir.push(PROGRAM_NAME);
    std::fs::create_dir_all(&data_dir).expect("Failed to create hyphy's data directory");
    data_dir
}
