#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

use cursive::align::HAlign;
use cursive::view::Scrollable;
use cursive::views::{Dialog, Panel, TextView};
use cursive::Cursive;

pub mod app;
pub mod config;
pub mod ui;

fn main() {
    env_logger::init();
    better_panic::install();
    setup_panic_hook();

    let user_config = config::get_config();
    let mut siv = cursive::default();

    // Set theme
    user_config.initialize_theme(&mut siv);

    // We can quit by pressing `q`
    siv.add_global_callback('q', Cursive::quit);
    ui::list_view(&mut siv);
    siv.run();
}

fn setup_panic_hook() {
    std::panic::set_hook(Box::new(|panic_info| {
        better_panic::Settings::auto().create_panic_handler()(panic_info);
    }));
}
