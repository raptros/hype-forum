#[derive(Debug, Deserialize, Serialize)]
pub struct App {
    pub server: String,
    pub subscribed_threads: Vec<u64>,
    pub database: String,
}
